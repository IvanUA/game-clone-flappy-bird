const block = document.getElementById("block");
const hole = document.getElementById("hole");
const bird = document.getElementById("bird");

let jumping = 0;
let counter = 0;
let intervalTIME = 10;

hole.addEventListener("animationiteration", () => {
  let random = -(Math.random() * 300 + 150);
  hole.style.top = random + "px";
  counter++;
});

setInterval(() => {
  let birdTOP = parseInt(window.getComputedStyle(bird).getPropertyValue("top"));

  if (jumping === 0) {
    bird.style.top = birdTOP + 3 + "px";
  }

  let blockLEFT = parseInt(
    window.getComputedStyle(block).getPropertyValue("left")
  );

  let holeTOP = parseInt(window.getComputedStyle(hole).getPropertyValue("top"));
  let bTop = -(500 - birdTOP);

  if (
    birdTOP > 480 ||
    (blockLEFT < 20 &&
      blockLEFT > -50 &&
      (bTop < holeTOP || bTop > holeTOP + 130))
  ) {
    alert("game over. Score: " + (counter - 1))
    bird.style.top = 100 + "px";
    counter = 0;
  }
}, intervalTIME);

const jump = () => {
    jumping = 1;
    let jumpCount = 0;
    const jumpInterval = setInterval(() => {
        let birdTOP = parseInt(window.getComputedStyle(bird).getPropertyValue("top"));

        if(birdTOP > 6 && jumpCount < 15) {
            bird.style.top = birdTOP - 5 + "px";
        }

        if(jumpCount > 20) {
            clearInterval(jumpInterval)
            jumping = 0;
            jumpCount = 0;
        }

        jumpCount++

    }, intervalTIME)
}